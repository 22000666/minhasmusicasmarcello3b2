//
//  ViewController.swift
//  minhasmusicasmarcello3b2
//
//  Created by COTEMIG on 18/08/22.
//

import UIKit

struct Song {
    let songName: String
    let albumName: String
    let singerName: String
    let songImageBig: String
    let songImageSmall: String
}

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var songList:[Song] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.songList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let song = self.songList[indexPath.row]
        
        cell.albumName.text = song.albumName
        cell.singerName.text = song.singerName
        cell.songName.text = song.songName
        cell.songImage.image = UIImage(named: song.songImageSmall)
        
        return cell
    }
    

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.dataSource = self
        tableView.delegate = self
        self.songList.append(Song(songName: "Pontos Cardeais", albumName: "Álbum vivo!", singerName: "Alceu Valença", songImageBig: "capa_alceu_grande", songImageSmall: "capa_alceu_pequeno"))
        self.songList.append(Song(songName:"Menor abandonado" , albumName:"Álbum Patota de Cosme" , singerName:"Zeca Pagodinho" , songImageBig:"capa_zeca_grande" , songImageSmall:"capa_zeca_pequeno" ))
        self.songList.append(Song(songName:"Tiro ao Álvaro" , albumName:"Álbum Adoniran Barbosa e Convidados" , singerName:"Adoniran Barbosa" , songImageBig:"capa_adhoniran_grande" , songImageSmall:"capa_adoniran_pequeno" ))
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        self.performSegue(withIdentifier: "abrirDetalhe", sender: indexPath.row)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let songDetailViewController = segue.destination as! SongDetailViewController
        let indice = sender as! Int
        let song = self.songList[indice]
        songDetailViewController.songImageBigStatic = song.songImageBig
        songDetailViewController.songSingerStatic = song.singerName
        songDetailViewController.songAlbumStatic = song.albumName
        songDetailViewController.songNameStatic = song.singerName
    }

}

